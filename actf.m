function res = actf(tact)
% sigmoid activation function
% tact - total activation 

	res = 2.*ones(size(tact)) ./ ( ones(size(tact)) + exp(-tact) ) - 1;

