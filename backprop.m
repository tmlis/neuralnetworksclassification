function [hidlw outlw terr] = backprop(tset, tslb, inihidlw, inioutlw, lr, dp)
% Function performs one epoch of backpropagation training
% with incremental (sample by sample) method
% tset - training set (every row represents a sample)
% tslb - column vector of labels 
% inihidlw - initial hidden layer weight matrix
% inioutlw - initial output layer weight matrix
% lr - learning rate
% dp - dropout probability

% hidlw - hidden layer weight matrix
% outlw - output layer weight matrix
% terr - total squared error of the ANN

n = size(inihidlw,2);  % no neurons in hidl
z = floor(dp*n);  % no neurons to exclude
drop = [zeros(z,1); ones(n-z,1)];

% 1. Set output matrices to initial values
	hidlw = inihidlw;
	outlw = inioutlw;
	
% 2. Set total error to 0
	terr = 0;
	
% foreach sample in the training set
	for i=1:rows(tset)
		% 3. Set desired output of the ANN
		% Note: I prefer bipolar outputs so "negative" answer of a neuron is -1
		% Don't forget to set hot output (desired class index)			
		dout = -1 * ones(1, columns(outlw));
		hout = tslb(i);  % desired class index
		dout(1, hout) = 1;
		
		% 4. Propagate input forward through the ANN
		% remember to extend input [tset(i, :) 1]
		drop = drop(1, randperm(size(drop,2)));  % random selection of neurons to drop
		
		acthl = actf([tset(i, :) 1] * hidlw);
		acthl(:, drop==0)=0;

		actol = actf([acthl 1] * outlw);


		% 5. Adjust total error (just to know this value)
		%terr = terr + 0.5 * sum((dout - actol).^2,2);  % Mean Square Error, also known as cost function
		
		% 6. Compute delta error of the output layer
		% how many delta errors should be computed here?   ->   for each neuron in output layer
		rold = (dout - actol).*actdf(actol);
		
		% 7. Compute delta error of the hidden layer
		% how many delta errors should be computed here?   ->   for each neuron in hidden layer, excluding dropped
		rhld = rold*outlw(1:end-1,:)'.*actdf(acthl);
		rhld(drop==0)=0;
		
		% 8. Update output layer weights
		outlw = outlw + lr*[acthl 1]' * rold;
		
		% 9. Update hidden layer weights
		hidlw = hidlw + lr*[tset(i, :) 1]' * rhld;
		
	end
