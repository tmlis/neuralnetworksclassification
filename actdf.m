function res = actdf(sfvalue)
% derivative of sigmoid activation function
% sfvalue - value of sigmoid activation function (!!!)

	res = 0.5.*ones(size(sfvalue)) .* ( ones(size(sfvalue)) - sfvalue.*sfvalue );
